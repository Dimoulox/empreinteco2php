<?php 

class Db_model extends CI_Model 
{

	public function __construct()
	{
		$this->load->database();
	}

	public function create_account($pseudo, $motDePasse, $nom, $prenom, $email)
	{
		$this->load->helper('url');
		$query = $this->db->query("INSERT INTO `COMPTE`(`com_pseudo`, `com_motDePasse`, `com_nom`, `com_prenom`, `com_mail`, `typCom_id`) VALUES ('". $pseudo ."','".$motDePasse."','".$nom."','".$prenom."','".$email."',3)");
	}

	public function check_account($pseudo)
	{
		$this->load->helper('url');
		$query = $this->db->query("SELECT com_pseudo, com_mail FROM `COMPTE` WHERE com_pseudo = '".$pseudo."';");

		return $query->row_array();
	}

	public function login_account($pseudo, $motDePasse)
	{
		$this->load->helper('url');
		$query = $this->db->query("SELECT typCom_id, typCom_nom, com_pseudo, com_motDePasse, com_nom, com_mail, com_prenom  FROM COMPTE INNER JOIN TYPE_COMPTE USING(typCom_id) WHERE com_pseudo = '".$pseudo."' AND com_motDePasse = '".$motDePasse."';");

		return $query->row_array();
	}

	public function update_data_success($pseudo, $nom, $prenom, $email, $nouveauMotDePasse)
	 {
	 	$this->load->helper('url');

	 	$query = $this->db->query("UPDATE `COMPTE` SET `com_motDePasse`='".$nouveauMotDePasse."',`com_nom`='".$nom."',`com_prenom`='".$prenom."',`com_mail`='".$email."' WHERE com_pseudo = '".$pseudo."'");
	 }


	public function get_equipment($pseudo)
	{
		$this->load->helper('url');
		$query = $this->db->query("SELECT equ_nom, con_date, con_utilisation, equ_consommationUnitaire, tau_valeur, equ_actif,  con_nbPersonne, CONSOMMATION.com_pseudo FROM EQUIPEMENT INNER JOIN CONSOMMATION USING(equ_id) INNER JOIN TAUX USING(tau_id) WHERE EQUIPEMENT.com_pseudo ='".$pseudo."';");

		return $query->result_array();	
	}

}

?>