<link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>style/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>style/css/main.css">

<div class="limiter">
		<div class="container-login100 my-5">
			
			<div class="wrap-login100">


				<div class="login100-form-title" style="background-image: url(<?php echo base_url(); ?>style/img/bg-img/bg-1.jpg);">
					<span class="login100-form-title-1">
						Connexion
					</span>
				</div>
				<?php //echo form_open('Login_register/login'); ?>
				<form class="login100-form validate-form" method="POST" action="login">
					<div class="wrap-input100 validate-input m-b-26" >
						<span class="label-input100">Pseudo</span>
						<input class="input100" type="text" name="pseudo" minlength="2" placeholder="Pseudo.." required="true">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-18" >
						<span class="label-input100">Mot de passe</span>
						<input class="input100" type="password" name="motDePasse" minlength="3" maxlength="12" placeholder="Mot de passe..." required="true">
						<span class="focus-input100"></span>
					</div>

					<div class="my-2 text-left">
							<a href="<?php echo base_url() ?>index.php/Compte/register" class="txt1">
								Vous n'etes pas inscrit ?
							</a>
						</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>