<?php

if (!isset($_SESSION['Particulier'])) {
  header("Location: ".base_url()."index.php/");
    exit();
}
?>


<div >

	<div class="text-center my-5">
		<h2>Bienvenue, <?php echo $_SESSION['Particulier']; ?></h2>
	</div>

</div>


<div class="row mb-5">
  <div class="col-sm-6 ">
    <div class="ml-5 mr-5">
      <div class="text-white text-center d-flex align-items-center">
        <a style="background-image: url('https://cdn.pixabay.com/photo/2016/01/19/17/15/wind-turbine-1149604__340.jpg'); background-repeat: no-repeat; background-size: cover; height: 500px; width: 100%" href="www.google.com">
    </a>
        
      </div>
      <a href="<?php echo base_url() ?>index.php/Consommation/consommation_mensuelle" class="btn btn-primary my-3 btn-block">Renseigner une consommation mensuelle</a>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="ml-5 mr-5">
      <div class="d-flex">
        <a style="background-image: url('https://cdn.pixabay.com/photo/2015/10/22/17/45/leaf-1001679__340.jpg'); background-repeat: no-repeat; background-size: cover; height: 500px; width: 100%" href="www.youtube.com">
    </a>
       
      </div>
       <a href="#" class="btn btn-primary my-3 btn-block">Mes consommation mensuelles</a>
    </div>
  </div>
</div>

