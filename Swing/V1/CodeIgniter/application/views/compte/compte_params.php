<?php

if (!isset($_SESSION['Particulier'])) {
  header("Location: ".base_url()."index.php/");
    exit();
}
?>


<div>

	<div class="text-center my-5">
		<h1>Paramètres du compte</h1>
	</div>


</div>

<div class="container" >
	<?php echo form_open('Compte/update_data'); ?>
	<form>
		
	  <div class="form-group row">
	    <label for="inputEmail3" class="col-sm-3 col-form-label">Nom</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="inputEmail3" name="nom" value="<?php echo $comptes['com_nom']; ?>">
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="inputEmail3" class="col-sm-3 col-form-label">Prénom</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="inputEmail3" name="prenom" value="<?php echo $comptes['com_prenom']; ?>">
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="inputEmail3" class="col-sm-3 col-form-label">Mail</label>
	    <div class="col-sm-5">
	      <input type="Mail" class="form-control" id="inputEmail3" name="e-mail" value="<?php echo $comptes['com_mail']; ?>">
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="inputEmail3" class="col-sm-3 col-form-label">Type de compte</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="inputEmail3" disabled="true" value="Particulier">
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="inputEmail3" class="col-sm-3 col-form-label">Pseudo</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="inputEmail3" disabled="true" name="pseudo" value="<?php echo $comptes['com_pseudo']; ?>">
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="inputPassword3" class="col-sm-3 col-form-label">Ancien mot de passe</label>
	    <div class="col-sm-5">
	      <input type="password" class="form-control" name="ancienMotDePasse" id="inputPassword3">
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="inputEmail3" class="col-sm-3 col-form-label">Nouveau mot de passe</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" name="nouveauMotDePasse" id="inputEmail3">
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="inputEmail3" class="col-sm-3 col-form-label">Confirmer le nouveau mot de passe</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control btn-lg" name="confirmationNouveauMotDePasse" id="inputEmail3">
	    </div>
	  </div>
	 
	  <div class="form-group row my-5">
	    <div class="col-sm-10 text-center">
	      <button type="submit" class="btn btn-primary">Sauvegarder les modifications</button>
	    </div>
	  </div>
	</form>

</div>