<?php

defined('BASEPATH') OR exit('No direct script acces allowed');

class Compte extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('db_model');
		$this->load->helper('url_helper');
		$this->load->helper('form');
		$this->load->library('form_validation');
		session_start();
	}

	public function login()
	{

		$this->form_validation->set_rules('pseudo', 'pseudo', 'required');
		$this->form_validation->set_rules('motDePasse', 'motDePasse', 'required');
		

		if($this->form_validation->run() == FALSE ){

			$this->load->view('templates/header_page');
			$this->load->view('login');
			$this->load->view('templates/footer');

		} else {

			$pseudo = addslashes($this->input->post('pseudo'));
			$motDePasse = addslashes($this->input->post('motDePasse'));

			$data['comptes'] = $this->db_model->login_account($pseudo, $motDePasse);


			if ( $data['comptes']['typCom_nom'] == "Gestionnaire") {

				
				$_SESSION['Gestionnaire'] = $pseudo;
				$_SESSION['motDePasse'] = $motDePasse;
				$this->load->view('templates/header_gestionnaire');
				$this->load->view('compte/compte_menu_gestionnaire',$data);
				$this->load->view('templates/footer');

			} else if ( $data['comptes']['typCom_nom'] == "Analyste") {

				$_SESSION['Analyste'] = $pseudo;
				$_SESSION['motDePasse'] = $motDePasse;
				$this->load->view('templates/header_analyste');
				$this->load->view('compte/compte_menu_analyste',$data);
				$this->load->view('templates/footer');

			} else if ( $data['comptes']['typCom_nom'] == "Particulier") {

				$_SESSION['Particulier'] = $pseudo;
				$_SESSION['motDePasse'] = $motDePasse;
				$this->load->view('templates/header_particulier');
				$this->load->view('compte/compte_menu_particulier',$data);
				$this->load->view('templates/footer');

			} else {

				$this->load->view('templates/header_page');
				$this->load->view('login');
				$this->load->view('compte/compte_erreur');
				$this->load->view('templates/footer');

			}

		}

		
		
	}

	public function register($validate = FALSE)
	{

		$this->form_validation->set_rules('nom', 'nom', 'required');
		$this->form_validation->set_rules('prenom', 'prenom', 'required');
		$this->form_validation->set_rules('e-mail', 'e-mail', 'required');
		$this->form_validation->set_rules('pseudo', 'pseudo', 'required');
		$this->form_validation->set_rules('motDePasse', 'motDePasse', 'required');
		$this->form_validation->set_rules('ConfirmationMotDePasse', 'ConfirmationMotDePasse', 'required');

		$nom = addslashes($this->input->post('nom'));
		$prenom = addslashes($this->input->post('prenom'));
		$email = addslashes($this->input->post('e-mail'));
		$pseudo = addslashes($this->input->post('pseudo'));
		$motDePasse = addslashes($this->input->post('motDePasse'));
		$ConfirmationMotDePasse = addslashes($this->input->post('ConfirmationMotDePasse'));
		

		if ($this->form_validation->run()==FALSE){

			$this->load->view('templates/header_page');
			$this->load->view('register');
			$this->load->view('templates/footer');
		}
		else{

			if($this->db_model->check_account($pseudo) > 0){

				$this->load->view('templates/header_page');
				$this->load->view('register');
				$this->load->view('compte/register_erreur');
				$this->load->view('templates/footer');


			} else {

				if($motDePasse == $ConfirmationMotDePasse) {

					$this->db_model->create_account($pseudo, $motDePasse, $ConfirmationMotDePasse, $nom, $prenom, $email);
				
					$this->load->view('templates/header_page');
					$this->load->view('compte/register_validate');
					$this->load->view('login');
					$this->load->view('templates/footer');

				} else {

					$this->load->view('templates/header_page');
					$this->load->view('register');
					$this->load->view('compte/register_erreur_password');
					$this->load->view('templates/footer');

				}
				
			}
			
		}
		
	}

	public function deconnexion(){
		session_start();
		session_unset();
	    session_destroy();
	    header("Location: ".base_url()."index.php/");
	    exit();
	}


	public function account_params()
	{

		$data['comptes'] = $this->db_model->login_account($_SESSION['Particulier'], $_SESSION['motDePasse']);

		if ( $data['comptes']['typCom_nom'] == "Particulier" ) {

				$this->load->view('templates/header_particulier');
				$this->load->view('compte/compte_params', $data);
				$this->load->view('templates/footer');

		} else {

			$this->load->view('templates/header_page');
			$this->load->view('login');
			$this->load->view('compte/compte_erreur');
			$this->load->view('templates/footer');

		}
	}

	public function update_data()
	{
		$this->form_validation->set_rules('nom', 'nom', 'required');
		$this->form_validation->set_rules('prenom', 'prenom', 'required');
		$this->form_validation->set_rules('e-mail', 'e-mail', 'required');
		$this->form_validation->set_rules('ancienMotDePasse', 'ancienMotDePasse', 'required');
		$this->form_validation->set_rules('nouveauMotDePasse', 'nouveauMotDePasse', 'required');
		$this->form_validation->set_rules('confirmationNouveauMotDePasse', 'confirmationNouveauMotDePasse', 'required');

		$nom = addslashes($this->input->post('nom'));
		$prenom = addslashes($this->input->post('prenom'));
		$pseudo = $_SESSION["Particulier"];
		$email = addslashes($this->input->post('e-mail'));
		$ancienMotDePasse = addslashes($this->input->post('ancienMotDePasse'));
		$nouveauMotDePasse = addslashes($this->input->post('nouveauMotDePasse'));
		$confirmationNouveauMotDePasse = addslashes($this->input->post('confirmationNouveauMotDePasse'));

		if($ancienMotDePasse == $_SESSION['motDePasse'] && $nouveauMotDePasse == $confirmationNouveauMotDePasse ){

			if(!($this->db_model->update_data_success($pseudo, $nom, $prenom, $email, $nouveauMotDePasse))){
				
				$this->load->view('templates/header_particulier');
				$this->load->view('compte/confirmation_update_data');
				$this->load->view('templates/footer');
				
			} else {
				echo "ERROR UPDATE DATA ";
				echo $pseudo. $nom. $prenom. $email. $nouveauMotDePasse;
				
			}

		} else {
			echo "ERROR UPDATE !!";
		}


	}

}

?>