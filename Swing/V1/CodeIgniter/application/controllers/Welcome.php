<?php

defined('BASEPATH') OR exit('No direct script acces allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('db_model');
		$this->load->helper('url_helper');
	}

	public function index()
	{
		
		
		$this->load->view('templates/header');
		//$this->load->view('index.php');
		$this->load->view('templates/footer');
		
	}

}

?>