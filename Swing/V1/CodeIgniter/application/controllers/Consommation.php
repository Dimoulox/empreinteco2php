<?php

defined('BASEPATH') OR exit('No direct script acces allowed');

class Consommation extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('db_model');
		$this->load->helper('url_helper');
		$this->load->helper('form');
		$this->load->library('form_validation');
		session_start();
	}


	public function consommation_mensuelle()
	{
		if($_SESSION['Particulier']){

			$data['equipements'] = $this->db_model->get_equipment($_SESSION['Particulier']);

		} else{

			$data['equipements'] = $this->db_model->get_equipment($_SESSION['Analyste']);

		}

		$this->load->view('templates/header_particulier');
		$this->load->view('consommation/display_consommation_mensuelle', $data);
		$this->load->view('templates/footer');
	}



}

?>
