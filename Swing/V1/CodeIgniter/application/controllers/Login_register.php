<?php

defined('BASEPATH') OR exit('No direct script acces allowed');

class Login_register extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('db_model');
		$this->load->helper('url_helper');
		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function login()
	{
		
		$this->load->view('templates/header');
		$this->load->view('login');
		$this->load->view('templates/footer');
		
	}

	public function register()
	{

		$this->form_validation->set_rules('nom', 'nom', 'required');
		$this->form_validation->set_rules('prenom', 'prenom', 'required');
		$this->form_validation->set_rules('e-mail', 'e-mail', 'required');
		$this->form_validation->set_rules('pseudo', 'pseudo', 'required');
		$this->form_validation->set_rules('motDePasse', 'motDePasse', 'required');
		$this->form_validation->set_rules('typeCompte', 'typeCompte', 'required');

		$nom = addslashes($this->input->post('nom'));
		$prenom = addslashes($this->input->post('prenom'));
		$email = addslashes($this->input->post('e-mail'));
		$pseudo = addslashes($this->input->post('pseudo'));
		$motDePasse = addslashes($this->input->post('motDePasse'));
		$typeCompte = addslashes($this->input->post('typeCompte'));
		

		if ($this->form_validation->run()==FALSE){

			$this->load->view('templates/header');
			$this->load->view('register');
			echo "ERROR REGISTER ";
			$this->load->view('templates/footer');
		}
		else{

			$this->db_model->create_account($pseudo, $motDePasse, $nom, $prenom, $email, $typeCompte);
			$this->load->view('templates/header');
			$this->load->view('compte_succes');
			$this->load->view('templates/footer');

		}
		
	}


}

?>