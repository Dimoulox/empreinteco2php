<?php

defined('BASEPATH') OR exit('No direct script acces allowed');

class Equipement extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('db_compte');
		$this->load->model('db_equipement');
		$this->load->model('db_categorie');
		$this->load->helper('url_helper');
		$this->load->helper('form');
		$this->load->library('form_validation');
		session_start();
	}

	/*
	 * Affiche le menu principal du compte.
	 * Si l'on tente d'accéder à la page sans s'être connecté,
	 * redirection sur la page de connexion avec un message d'erreur.
	 */
	public function afficherEquipements() {
		$compte = $this->db_compte->login_account($_SESSION['Particulier'], $_SESSION['motDePasse']);

		if ($compte['typCom_nom'] == "Particulier") {
			$data['equipements'] = $this->db_equipement->getEquipementsFromAccount($_SESSION['Particulier']);
			$data['categories'] = $this->db_categorie->getAllCategories();
			
			$this->load->view('templates/header_particulier');
			$this->load->view('compte/compte_equipements',$data);
			//$this->load->view('compte/compte_equipements');
			$this->load->view('templates/footer');
		}
		else {
			$this->load->view('templates/header_page');
			$this->load->view('login');
			$this->load->view('compte/compte_erreur');
			$this->load->view('templates/footer');
		}
	}

	/*
	 * Inscription d'un particulier.
	 * Si le pseudo entré n'existe pas déjà et que le mot de passe et sa confirmation
	 * sont les même, le compte est créé et l'on est redirigé sur la page de connexion
	 * avec un message de confirmation.
	 *
	 * Sinon, recharge la page et affiche un message d'erreur.
	 */
	public function inscription($validate = FALSE) {

		$this->form_validation->set_rules('nom', 'nom', 'required');
		$this->form_validation->set_rules('prenom', 'prenom', 'required');
		$this->form_validation->set_rules('e-mail', 'e-mail', 'required');
		$this->form_validation->set_rules('pseudo', 'pseudo', 'required');
		$this->form_validation->set_rules('motDePasse', 'motDePasse', 'required');
		$this->form_validation->set_rules('ConfirmationMotDePasse', 'ConfirmationMotDePasse', 'required');

		$nom = addslashes($this->input->post('nom'));
		$prenom = addslashes($this->input->post('prenom'));
		$email = addslashes($this->input->post('e-mail'));
		$pseudo = addslashes($this->input->post('pseudo'));
		$motDePasse = addslashes($this->input->post('motDePasse'));
		$ConfirmationMotDePasse = addslashes($this->input->post('ConfirmationMotDePasse'));
		

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('templates/header_page');
			$this->load->view('register');
			$this->load->view('templates/footer');
		}
		else {
			// On vérifie si le pseudo entré n'existe pas déjà
			if ($this->db_compte->check_account($pseudo) > 0) {
				$this->load->view('templates/header_page');
				$this->load->view('compte/register_erreur');
				$this->load->view('register');
				$this->load->view('templates/footer');
			}
			else {
				// On vérifie si le mot de passe et sa confirmation sont bien les mêmes
				if ($motDePasse == $ConfirmationMotDePasse) {
					$this->db_compte->create_account($pseudo, $motDePasse, $ConfirmationMotDePasse, $nom, $prenom, $email);
				
					$this->load->view('templates/header_page');
					$this->load->view('compte/register_validate');
					$this->load->view('login');
					$this->load->view('templates/footer');
				}
				else {
					$this->load->view('templates/header_page');
					$this->load->view('compte/register_erreur_password');
					$this->load->view('register');
					$this->load->view('templates/footer');
				}	
			}
		}
	}

	/*
	 * Connexion d'un particulier.
	 * Si le pseudo et le mot de passe entrés correspondent à un compte
	 * dans la base de donnée, connecte l'utilisateur.
	 *
	 * Sinon, recharge la page et affiche un message d'erreur.
	 */
	public function login() {

		$this->form_validation->set_rules('pseudo', 'pseudo', 'required');
		$this->form_validation->set_rules('motDePasse', 'motDePasse', 'required');
		
		// Vérifie si les champs obligatoires sont bien remplis
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('templates/header_page');
			$this->load->view('login');
			$this->load->view('templates/footer');
		}
		else {
			// Ajout d'antislashes afin de gérer les quotes
			$pseudo = addslashes($this->input->post('pseudo'));
			$motDePasse = addslashes($this->input->post('motDePasse'));

			$data['comptes'] = $this->db_compte->login_account($pseudo, $motDePasse);

			if ($data['comptes']['typCom_nom'] == "Gestionnaire") {
				$_SESSION['Gestionnaire'] = $pseudo;
				$_SESSION['motDePasse'] = $motDePasse;

				$this->load->view('templates/header_gestionnaire');
				$this->load->view('compte/compte_menu_gestionnaire',$data);
				$this->load->view('templates/footer');
			}
			else if ($data['comptes']['typCom_nom'] == "Analyste") {
				$_SESSION['Analyste'] = $pseudo;
				$_SESSION['motDePasse'] = $motDePasse;

				$this->load->view('templates/header_analyste');
				$this->load->view('compte/compte_menu_analyste',$data);
				$this->load->view('templates/footer');
			}
			else if ($data['comptes']['typCom_nom'] == "Particulier") {
				$_SESSION['Particulier'] = $pseudo;
				$_SESSION['motDePasse'] = $motDePasse;

				$this->load->view('templates/header_particulier');
				$this->load->view('compte/compte_menu_particulier',$data);
				$this->load->view('templates/footer');
			}
			else {
				$this->load->view('templates/header_page');
				$this->load->view('compte/compte_erreur');
				$this->load->view('login');
				$this->load->view('templates/footer');
			}
		}	
	}

	/*
	 * Déconnexion de l'utilisateur et redirection sur la page d'accueil.
	 */
	public function deconnexion(){
		session_start();
		session_unset();
	    session_destroy();
	    header("Location: ".base_url()."index.php/");
	    exit();
	}

	/*
	 * Affiche les paramètres du compte.
	 * Si l'on tente d'accéder à la page sans s'être connecté,
	 * redirection sur la page de connexion avec un message d'erreur.
	 */
	public function afficherParametresCompte($messageModif = NULL) {
		$data['comptes'] = $this->db_compte->login_account($_SESSION['Particulier'], $_SESSION['motDePasse']);

		if ($data['comptes']['typCom_nom'] == "Particulier") {
			$this->load->view('templates/header_particulier');
			if ($messageModif != NULL) {
				$this->load->view($messageModif);
			}
			$this->load->view('compte/compte_params', $data);
			$this->load->view('templates/footer');
		}
		else {
			$this->load->view('templates/header_page');
			$this->load->view('compte/compte_erreur');
			$this->load->view('login');
			$this->load->view('templates/footer');
		}
	}

	/*
	 * Modification des paramètres du compte.
	 * Si l'on tente d'accéder à cette méthode sans s'être connecté,
	 * redirection sur la page de connexion avec un message d'erreur.
	 */
	public function update_data() {
		$this->form_validation->set_rules('nom', 'nom', 'required');
		$this->form_validation->set_rules('prenom', 'prenom', 'required');
		$this->form_validation->set_rules('e-mail', 'e-mail', 'required');
		$this->form_validation->set_rules('ancienMotDePasse', 'ancienMotDePasse', 'required');
		$this->form_validation->set_rules('nouveauMotDePasse', 'nouveauMotDePasse', 'required');
		$this->form_validation->set_rules('confirmationNouveauMotDePasse', 'confirmationNouveauMotDePasse', 'required');

		// Ajout d'antislashes afin de gérer les quotes
		$nom = addslashes($this->input->post('nom'));
		$prenom = addslashes($this->input->post('prenom'));
		$pseudo = $_SESSION["Particulier"];
		$email = addslashes($this->input->post('e-mail'));
		$ancienMotDePasse = addslashes($this->input->post('ancienMotDePasse'));
		$nouveauMotDePasse = addslashes($this->input->post('nouveauMotDePasse'));
		$confirmationNouveauMotDePasse = addslashes($this->input->post('confirmationNouveauMotDePasse'));

		if (empty($ancienMotDePasse) && empty($nouveauMotDePasse) && empty($confirmationNouveauMotDePasse)) {
			$this->db_compte->update_compte($pseudo, $nom, $prenom, $email, $_SESSION['motDePasse']);
			$this->afficherParametresCompte('compte/confirmation_update_data');
		}
		else {
			if ($ancienMotDePasse == $_SESSION['motDePasse']
				&& $nouveauMotDePasse == $confirmationNouveauMotDePasse
				&& !empty($nouveauMotDePasse)) {
				$this->db_compte->update_compte($pseudo, $nom, $prenom, $email, $nouveauMotDePasse);
				$_SESSION['motDePasse'] = $nouveauMotDePasse;
				$this->afficherParametresCompte('compte/confirmation_update_data');
			}
			else {
				$this->afficherParametresCompte('compte/error_update_data');
			}
		}
	}
}

?>