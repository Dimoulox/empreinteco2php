<?php

defined('BASEPATH') OR exit('No direct script acces allowed');

class Consommation extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('db_compte');
		$this->load->model('db_consommation');
		$this->load->model('db_equipement');
		$this->load->helper('url_helper');
		$this->load->helper('form');
		$this->load->library('form_validation');
		session_start();
	}


	public function consommation_mensuelle($mode = FALSE)
	{
		if(!$mode){

			if($_SESSION['Particulier']){

				$data['equipements'] = $this->db_compte->get_equipment($_SESSION['Particulier']);
				$data['equipementsChoix'] = $this->db_equipement->getEquipementsFromAccountAndPublic($_SESSION['Particulier']);

				$this->load->view('templates/header_particulier');
				$this->load->view('consommation/display_consommation_mensuelle', $data);
				$this->load->view('templates/footer');

			} else{

				$data['equipements'] = $this->db_compte->get_equipment($_SESSION['Analyste']);

				$this->load->view('templates/header_particulier');
				$this->load->view('consommation/display_consommation_mensuelle', $data);
				$this->load->view('templates/footer');

			}

		} else {

			if($_SESSION['Particulier']){

				$data['equipements'] = $this->db_compte->get_equipment($_SESSION['Particulier']);
				$data['equipementsChoix'] = $this->db_equipement->getEquipementsFromAccountAndPublic($_SESSION['Particulier']);

				$this->load->view('templates/header_particulier');
				$this->load->view('consommation/form_success');
				$this->load->view('consommation/display_consommation_mensuelle', $data);
				$this->load->view('templates/footer');

			} else{

				$data['equipements'] = $this->db_compte->get_equipment($_SESSION['Analyste']);

				$this->load->view('templates/header_particulier');
				$this->load->view('consommation/form_success');
				$this->load->view('consommation/display_consommation_mensuelle', $data);
				$this->load->view('templates/footer');

			}

		}

		
	}

	public function insertion_consommation()
	{
		$this->form_validation->set_rules('equipement', 'equipement', 'required');
		$this->form_validation->set_rules('utilisation', 'utilisation', 'required');
		$this->form_validation->set_rules('date', 'date', 'required');
		$this->form_validation->set_rules('nbPersonnes', 'nbPersonnes', 'required');


		$pseudo = $_SESSION["Particulier"];
		$idEquipement = $this->input->post('equipement');
		$utilisation = $this->input->post('utilisation');
		$date = $this->input->post('date');
		$nombrePersonne = $this->input->post('nbPersonnes');
	
		

		if($_SESSION['Particulier']){

			$data['equipements'] = $this->db_compte->get_equipment($_SESSION['Particulier']);
			$data['equipementsChoix'] = $this->db_equipement->getEquipementsFromAccountAndPublic($_SESSION['Particulier']);

		} else{

			$data['equipements'] = $this->db_compte->get_equipment($_SESSION['Analyste']);

		}

		if($nombrePersonne == NULL){

		
			$nombrePersonne = 1;

			$data['consommation'] = $this->db_consommation->add_conso($utilisation, $date, $nombrePersonne, $pseudo, $idEquipement);
			
			$this->load->view('templates/header_particulier');
			$this->load->view('consommation/display_consommation_mensuelle', $data);
			$this->load->view('templates/footer');
			redirect('Consommation/consommation_mensuelle/'. $mode = TRUE);
			exit();

		} else {

			$data['consommation'] = $this->db_consommation->add_conso($utilisation, $date, $nombrePersonne, $pseudo, $idEquipement);

			$this->load->view('templates/header_particulier');
			$this->load->view('consommation/display_consommation_mensuelle', $data);
			$this->load->view('templates/footer');
			redirect('Consommation/consommation_mensuelle/'.$mode = TRUE);
			exit();
		}
	}


	public function maintenance()
	{

		$this->load->view('templates/header_particulier');
		$this->load->view('consommation/maintenance');
		$this->load->view('templates/footer');

	}



}

?>
