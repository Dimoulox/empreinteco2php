<?php 

class Db_equipement extends CI_Model  {

	public function __construct() {
		$this->load->database();
	}

	public function getEquipementsFromAccount($pseudo) {
		$this->load->helper('url');
		$requete = "SELECT equ_id,
							equ_nom,
							equ_consommationUnitaire,
							cat_id,
							cat_nom,
							tau_valeur,
							tau_nomTaux
					FROM EQUIPEMENT
					INNER JOIN CATEGORIE
						USING (cat_id)
					INNER JOIN TAUX
						USING (tau_id)
					WHERE com_pseudo = '".$pseudo."'
					AND equ_actif = 1
					ORDER BY cat_id;";

		$query = $this->db->query($requete);
		return $query->result_array();
	}

	public function getEquipementsFromAccountByCategory($pseudo, $categorie) {
		$this->load->helper('url');
		$requete = "SELECT equ_id,
							equ_nom,
							equ_consommationUnitaire,
							tau_valeur,
							tau_nomTaux
					FROM EQUIPEMENT
					INNER JOIN TAUX
						USING (tau_id)
					WHERE com_pseudo = '".$pseudo."'
					AND cat_id = ".$categorie."
					AND equ_actif = 1;";

		$query = $this->db->query($requete);
		return $query->result_array();
	}

	public function create_equipement($nom, $consommationUnitaire, $pseudo, $categorie, $taux) {
		$requete = "INSERT INTO `EQUIPEMENT`(`equ_nom`,
										`equ_consommationUnitaire`,
										`com_pseudo`,
										`cat_id`,
										`tau_id`,
										`equ_actif`)
					VALUES ('".$nom."',
							".$consommationUnitaire.",
							'".$pseudo."',
							".$categorie.",
							".$taux.",
							1);";

		$query = $this->db->query($requete);
		return $query;
	}

	public function getEquipementsFromAccountAndPublic($pseudo) {
		$this->load->helper('url');
		$requete = "SELECT equ_id,
							equ_nom,
							equ_consommationUnitaire,
							cat_id,
							cat_nom,
							tau_valeur,
							tau_nomTaux,
							com_pseudo
					FROM EQUIPEMENT
					INNER JOIN CATEGORIE
						USING (cat_id)
					INNER JOIN TAUX
						USING (tau_id)
					WHERE EQUIPEMENT.com_pseudo = '".$pseudo."'
					OR EQUIPEMENT.com_pseudo IS NULL
					AND equ_actif = 1
					ORDER BY cat_id;";

		$query = $this->db->query($requete);
		return $query->result_array();
	}
	/*
	public function update_compte($pseudo, $nom, $prenom, $email, $nouveauMotDePasse) {
	 	$this->load->helper('url');

	 	$requete = "UPDATE `COMPTE`
	 				SET `com_motDePasse`='".$nouveauMotDePasse."',
		 				`com_nom`='".$nom."',
		 				`com_prenom`='".$prenom."',
		 				`com_mail`='".$email."'
	 				WHERE com_pseudo = '".$pseudo."';";

	 	$query = $this->db->query($requete);
	 	return $query;
	}
	*/
}

?>