<?php 

class Db_categorie extends CI_Model  {

	public function __construct() {
		$this->load->database();
	}

	public function getAllCategories() {
		$requete = "SELECT cat_id,
							cat_nom
					FROM CATEGORIE
					ORDER BY cat_id;";

		$query = $this->db->query($requete);
		return $query->result_array();
	}

}

?>