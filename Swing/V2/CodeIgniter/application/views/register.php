
<link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>style/css/util.css">
<link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>style/css/main.css">


<?php //echo form_open('Login_register/register'); ?>
<div class="limiter">
		<div class="container-login100 my-5">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(<?php echo base_url(); ?>style/img/bg-img/bg-1.jpg);">
					<span class="login100-form-title-1">
						Inscription
					</span>
				</div>

				<form class="login100-form validate-form" method="post" action="inscription">
					<div class="wrap-input100 validate-input m-b-26" >
						<span class="label-input100">Nom</span>
						<input class="input100" type="text" name="nom" minlength="2" placeholder="Nom.." required="true">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-26" >
						<span class="label-input100">Prenom</span>
						<input class="input100" type="text" name="prenom" minlength="2" placeholder="Prenom.." required="true">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-26" >
						<span class="label-input100">Pseudo</span>
						<input class="input100" type="text" name="pseudo" minlength="2" placeholder="Pseudo.." required="true">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-26">
						<span class="label-input100">E-mail</span>
						<input class="input100" type="email" name="e-mail" placeholder="E-mail.." required="true">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-18" >
						<span class="label-input100">Mot de passe</span>
						<input class="input100" type="password" name="motDePasse" minlength="3" maxlength="12" placeholder="Mot de passe..." required="true">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-18" >
						<span class="label-input100">Confirmation mot de passe</span>
						<input class="input100" type="password" name="ConfirmationMotDePasse" minlength="3" maxlength="12" placeholder="Confirmation mot de passe..." required="true">
						<span class="focus-input100"></span>
					</div>

					<div class="my-2">
						<a href="<?php echo base_url() ?>index.php/Compte/login" class="txt1">
							Vous etes inscrit ?
						</a>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit">
							Créer
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>