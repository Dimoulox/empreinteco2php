<?php

date_default_timezone_set('UTC');

if (!isset($_SESSION['Particulier'])) {
  header("Location: ".base_url()."index.php/");
    exit();
}

$_SESSION["lastDate"] = date("Y-m-d");

?>



<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">


<div class="container">

<div class="text-left my-3">
  <a href="<?php echo base_url() ?>index.php/Compte/afficherCompte">Mon compte</a> > Mes équipements
</div>

  <h2 class="text-center mb-5">Renseigner une consommation mensuelle</h2>

<form method="POST">
<div class="form-group row">
  <label for="example-date-input" class="col-2 col-form-label">Mois à renseigner : </label>
  <div class="col-3">
    <input class="form-control" type="date" name="dateNow" value="<?php if(!isset($_POST["dateNow"])){ echo $_SESSION["lastDate"]; } else { echo $_POST["dateNow"]; } ?>"  id="example-date-input">
    
  </div>
  <button type="submit" class="btn btn-primary">Valider</button>
</div>
</form>


</div>
<div class="container">
  <ul class="nav nav-tabs md-tabs" id="myTabEx" role="tablist">
  <li class="nav-item">
    <a class="nav-link active show" id="home-tab-ex" data-toggle="tab" href="#transport" role="tab" aria-controls="transport"
      aria-selected="true">Transport</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab-ex" data-toggle="tab" href="#chauffage" role="tab" aria-controls="chauffage"
      aria-selected="false">Chauffage</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="contact-tab-ex" data-toggle="tab" href="#logement" role="tab" aria-controls="logement"
      aria-selected="false">Logement</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="contact-tab-ex" data-toggle="tab" href="#nourriture" role="tab" aria-controls="nourriture"
      aria-selected="false">Nourriture</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="contact-tab-ex" data-toggle="tab" href="#consommation" role="tab" aria-controls="consommation"
      aria-selected="false">Consommation</a>
  </li>
</ul>
<div class="tab-content pt-5" id="myTabContentEx">
  <div class="tab-pane fade active show" id="transport" role="tabpanel" aria-labelledby="home-tab-ex">
    <div class="col-sm-12 text-center my-2">
   
     <button type="button" class="rounded-circle btn-floating btn-lg btn-success" data-toggle="modal" data-target="#exampleModalCenter"><i class="glyphicon glyphicon-plus"></i>+</button>Ajouter un transport
 
    </div>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="padding: 20px;">
     
      <div class="text-center">
          <div class="form-check">
            <input class="form-check-input" type="radio" name="exampleRadios" id="radioTransportsCommun" value="option1" onchange="AfficherMasquerForms()" checked>
            <label class="form-check-label" for="exampleRadios1">
              Transport en commun
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="exampleRadios" id="radioMesTransports" value="option2" onchange="AfficherMasquerForms()">
            <label class="form-check-label" for="exampleRadios2">
              Mes transports
            </label>
          </div>
      </div>

      <div class="container" id="transports-commun">
        <?php echo form_open('Consommation/insertion_consommation'); ?>
        <form >
          <div class="form-group text-center my-2">
            <label  class="col-12" for="exampleInputEmail1">Quel transport en commun avez-vous prit ?</label>
              <select name="equipement" id="transportsCommun">
                <?php
                foreach ($equipementsChoix as $equ) {
                  if ($equ['cat_id'] == 1 && !isset($equ['com_pseudo'])) {
                    echo '<option  value="'.$equ['equ_id'].'">'.$equ['equ_nom'].'</option>';
                  }
                }
                ?>
              </select>
          </div>

          <div class="form-group text-center my-2">
            <label for="example-date-input" class="col-2 col-form-label">Date</label>
            <div class="text-center col-6" style="margin-left: 25%;">
              <input class="form-control" name="date" type="date" value="<?php if(!isset($_POST["dateNow"])){ echo $_SESSION["lastDate"]; } else { echo $_POST["dateNow"]; } ?>" id="example-date-input">
            </div>
          </div>

          <div class="form-group text-center my-2">
            <label class="col-12" for="exampleInputEmail1">Combien de kilomètres avez-vous parcouru ?</label>
              <input type="number" id="tentacles" name="utilisation" min="1" max="20000">
          </div>

         <div class="text-center">
            <button type="submit"  class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>

      <div class="container" id="mes-transports" style="display: none;">
        <?php echo form_open('Consommation/insertion_consommation'); ?>
        <form  >
          <div class="form-group text-center my-2">
            <label class="col-12" for="exampleInputEmail1">Lequel de vos transports avez-vous prit ?</label>
              <select name="equipement" id="mesTransports">
                <?php
                foreach ($equipementsChoix as $equ) {
                  var_dump($equ['com_pseudo']);
                  if ($equ['cat_id'] == 1 && isset($equ['com_pseudo'])) {
                    echo '<option value="'.$equ['equ_id'].'">'.$equ['equ_nom'].'</option>';
                  }
                }
                ?>
              </select>
          </div>

          <div class="form-group text-center my-2">
            <label for="example-date-input" class="col-2 col-form-label ">Date</label>
            <div class="col-6" style="margin-left: 25%;">
              <input class="form-control" name="date" type="date" value="<?php if(!isset($_POST["dateNow"])){ echo $_SESSION["lastDate"]; } else { echo $_POST["dateNow"]; } ?>" id="example-date-input">
            </div>
          </div>

          <div class="form-group text-center my-2">
            <label class="col-12" for="exampleInputEmail1">Combien de kilomètres avez-vous parcouru ?</label>
              <input type="number" id="tentacles" name="utilisation" min="1" max="20000">
          </div>

          <div class="form-group text-center my-2">
            <label class="col-12" for="exampleInputEmail1">Combien de personnes étiez-vous dans le véhicule ?</label>
              <input type="number" id="tentacles" name="nbPersonnes" min="1" max="20000">

          </div>

          <div class="text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>

      <script type="text/javascript">
        function AfficherMasquerForms() {
          formTransportsCommun = document.getElementById('transports-commun');
          formMesTransports = document.getElementById('mes-transports');
          radioTransportsCommun = document.getElementById('radioTransportsCommun');
          if (radioTransportsCommun.checked) {
            formTransportsCommun.style.display = "block";
            formMesTransports.style.display = "none";
          }
          else {
            formTransportsCommun.style.display = "none";
            formMesTransports.style.display = "block";
          }
        }
      </script>

    </div>
  </div>
</div>
   
    <table class="table col-6 container">
      <h2 class="text-center my-4">Transports en commun</h2>
      <thead class="thead-dark">
        <tr>
          <th scope="col">Nom</th>
          <th scope="col">Date</th>
          <th scope="col">Distance(Km)</th>
          <th scope="col">Classe</th>
          <th scope="col">Empreinte CO2</th>
        </tr>
      </thead>
      <tbody>
        
        <?php 
        
        if(!isset($_POST["dateNow"])){
          $_POST["dateNow"] = date("Y-m-d");
        }

     
    foreach($equipements as $equipement) {

      if($equipement["con_nbPersonne"] == 1){ 
      if(!isset($_POST["dateNow"])){
          $_POST["dateNow"] = date("Y-m-d");

            $dateNowExploded = explode("-",  $_POST["dateNow"]);

        } else {
          $dateNowExploded = explode("-",  $_POST["dateNow"]);
        }

        $dateConsommationExploded= explode("-", $equipement['con_date']);

    if (strcmp($dateNowExploded[0], $dateConsommationExploded[0]) == 0
       && strcmp($dateNowExploded[1], $dateConsommationExploded[1]) == 0) {
    
        ?>
      
      <tr>
        <td><?php echo $equipement["equ_nom"]; ?></td>
        <td><?php echo $equipement["con_date"]; ?></td>
        <td><?php echo $equipement["con_utilisation"]; ?></td>
        <td><?php if($equipement["equ_id"] == 6 || $equipement["equ_id"] == 7){ echo "Business";}else { echo "éco";} ?></td>
        <td>0</td>
      </tr> 

  <?php } } } ?>
      </tbody>
    </table>

    <table class="table col-8 container">
      <h2 class="text-center my-4">Mes transports</h2>
      <thead class="thead-dark">
        <tr>
          <th scope="col">Nom</th>
          <th scope="col">Date</th>
          <th scope="col">Distance(Km)</th>
          <th scope="col">Consommation unitaire(L/Km)</th>
          <th scope="col">Taux</th>
          <th scope="col">Nombre de passagers</th>
          <th scope="col">Empreinte CO2</th>
        </tr>
      </thead>
      <tbody>
         <?php 

      if(!isset($_POST["dateNow"])){
          $_POST["dateNow"] = date("Y-m-d");
        }

     foreach($equipements as $equipement) {

      if($equipement["con_nbPersonne"] != 1){ 
        if(!isset($_POST["dateNow"])){
            $_POST["dateNow"] = date("Y-m-d");

              $dateNowExploded = explode("-",  $_POST["dateNow"]);

          } else {
            $dateNowExploded = explode("-",  $_POST["dateNow"]);
          }

          $dateConsommationExploded= explode("-", $equipement['con_date']);

      if (strcmp($dateNowExploded[0], $dateConsommationExploded[0]) == 0
         && strcmp($dateNowExploded[1], $dateConsommationExploded[1]) == 0) {

          ?>
        
        <tr>
          <td><?php echo $equipement["equ_nom"]; ?></td>
          <td><?php echo $equipement["con_date"]; ?></td>
          <td><?php echo $equipement["con_utilisation"]; ?></td>
          <td><?php echo $equipement["equ_consommationUnitaire"]; ?></td>
          <td><?php echo $equipement["tau_valeur"]; ?></td>
          <td><?php echo $equipement["con_nbPersonne"]; ?></td>
          <td>0</td>
        </tr> 

  <?php } } } ?>
      </tbody>
    </table>

    <div class="col-sm-12 text-center my-5">
        <button type="submit" class="btn btn-primary">Sauvegarder les modifications</button>
      </div>

  </div>
  <div class="tab-pane fade" id="chauffage" role="tabpanel" aria-labelledby="profile-tab-ex">
    <h2 class="text-center my-5">chauffage</h2>
  </div>
  <div class="tab-pane fade" id="logement" role="tabpanel" aria-labelledby="contact-tab-ex">
    <h2 class="text-center my-5">logement</h2>
  </div>
  <div class="tab-pane fade" id="nourriture" role="tabpanel" aria-labelledby="contact-tab-ex">
    <h2 class="text-center my-5">nourriture</h2>
  </div>
  <div class="tab-pane fade" id="consommation" role="tabpanel" aria-labelledby="contact-tab-ex">
    <h2 class="text-center my-5">Consommation</h2>
  </div>
</div>
</div>