<div class="container text-center my-5">
	<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:inline-block;">
		Les informations du compte ont été modifiées.
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    	<span aria-hidden="true">&times;</span>
	  	</button>
	</div>
</div>