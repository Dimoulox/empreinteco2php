<div class="container text-center my-5">
	<div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:inline-block;">
		Le nom d'utilisateur et le mot de passe que vous avez entrés ne correspondent pas à ceux présents dans nos fichiers. Veuillez réessayer.
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    	<span aria-hidden="true">&times;</span>
	  	</button>
	</div>
</div>