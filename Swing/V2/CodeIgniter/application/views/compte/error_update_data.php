<div class="container text-center my-5">
	<div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:inline-block;">
		Un ou plusieurs des mots de passes entrés ne correspondent pas. Veuillez réessayer.
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    	<span aria-hidden="true">&times;</span>
	  	</button>
	</div>
</div>