<div class="container text-center my-5">
	<div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:inline-block;">
		Les 2 mots de passe sont différents. Veuillez vérifier et réessayer.
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    	<span aria-hidden="true">&times;</span>
	  	</button>
	</div>
</div>