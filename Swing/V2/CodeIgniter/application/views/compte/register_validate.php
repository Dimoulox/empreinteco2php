<div class="container text-center my-5">
	<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:inline-block;">
		Votre compte a bien été créé.
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    	<span aria-hidden="true">&times;</span>
	  	</button>
	</div>
</div>