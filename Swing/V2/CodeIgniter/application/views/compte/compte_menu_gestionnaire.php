<?php

if (!isset($_SESSION['Gestionnaire'])) {
  header("Location: ".base_url()."index.php/");
    exit();
}
?>

<div class="text-center my-5">
	<h2>Bienvenue <?php echo $_SESSION['Gestionnaire']; ?></h2>
</div>