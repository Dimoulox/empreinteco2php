<?php

if (!isset($_SESSION['Particulier'])) {
	header("Location: ".base_url()."index.php/");
	exit();
}
?>


<div>
	<div class="text-center">
		<h1>Mes équipements</h1>
	</div>
</div>

<div class="container text-left my-5">
	<a href="<?php echo base_url() ?>index.php/Compte/afficherCompte">Mon compte</a> > Mes équipements
</div>



<div class="container" >

	<ul class="nav nav-tabs md-tabs" id="myTab" role="tablist">
		<?php
		foreach ($categories as $cat) {
			echo '<li class="nav-item">';
			if ($cat['cat_id'] == 1) {
				echo 	'<a class="nav-link active show" id="'.$cat['cat_nom'].'-tab" data-toggle="tab" href="#'.$cat['cat_nom'].'" role="tab" aria-controls="'.$cat['cat_nom'].'" aria-selected="true">'.$cat['cat_nom'].'</a>';
			}
			else {
				echo 	'<a class="nav-link" id="'.$cat['cat_nom'].'-tab" data-toggle="tab" href="#'.$cat['cat_nom'].'" role="tab" aria-controls="'.$cat['cat_nom'].'" aria-selected="false">'.$cat['cat_nom'].'</a>';
			}
			echo '</li>';
		}
		?>
	</ul>

	<div class="tab-content" id="myTabContent">
		<?php
		foreach ($categories as $cat) {
			if ($cat['cat_id'] == 1) {
				echo '<div class="tab-pane fade show active" id="'.$cat['cat_nom'].'" role="tabpanel" aria-labelledby="'.$cat['cat_nom'].'-tab">';
			}
			else {
				echo '<div class="tab-pane fade" id="'.$cat['cat_nom'].'" role="tabpanel" aria-labelledby="'.$cat['cat_nom'].'-tab">';
			}
			echo 	'<table class="table">';
			echo 		'<thead class="thead-dark">';
			echo 			'<tr>';
			echo 				'<th scope="col">Nom</th>';
			echo 				'<th scope="col">Consommation unitaire</th>';
			echo 				'<th scope="col">Taux</th>';
			echo 			'</tr>';
			echo 		'</thead>';
			echo 		'<tbody>';
			foreach ($equipements as $equi) {
				if ($equi["cat_id"] == $cat["cat_id"]) {
					echo '<tr>';
					echo 	'<td>'.$equi['equ_nom'].'</td>';
					echo 	'<td>'.$equi['equ_consommationUnitaire'].'</td>';
					echo 	'<td>'.$equi['tau_nomTaux'].' ('.$equi['tau_valeur'].')</td>';
					echo '</tr>';
				}
			}
			echo 		'</tbody>';
			echo 	'</table>';
			echo '</div>';
		}
		?>
	</div>


	<!-- <?php echo form_open('Compte/update_data'); ?>
	<form>
		
		<div class="form-group row">
			<label for="nom" class="col-sm-3 col-form-label">Nom</label>
			<div class="col-sm-5">
				<input type="text" class="form-control" id="nom" name="nom">
			</div>
		</div>

		<div class="form-group row">
			<label for="prenom" class="col-sm-3 col-form-label">Consommation unitaire</label>
			<div class="col-sm-5">
				<input type="text" class="form-control" id="prenom" name="prenom">
			</div>
		</div>

		<div class="form-group row">
			<label for="e-mail" class="col-sm-3 col-form-label">Taux</label>
			<div class="col-sm-5">
				<input type="Mail" class="form-control" id="e-mail" name="e-mail">
			</div>
		</div>

		<div class="form-group row">
			<label for="typeCompte" class="col-sm-3 col-form-label">Catégorie</label>
			<div class="col-sm-5">
				<input type="text" class="form-control" id="typeCompte" disabled="true">
			</div>
		</div>

		<div class="form-group row my-5">
			<div class="col-sm-10 text-center">
				<button type="submit" class="btn btn-primary">Sauvegarder les modifications</button>
			</div>
		</div>
	</form> -->

</div>