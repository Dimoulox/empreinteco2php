<?php

if (!isset($_SESSION['Particulier'])) {
  header("Location: ".base_url()."index.php/");
    exit();
}

date_default_timezone_set('UTC');

?>


<div >

	<div class="text-center my-5">
		<h2>Bienvenue, <?php echo $_SESSION['Particulier']; ?></h2>
	</div>

</div>


<div class="row justify-content-md-center">
  <div class="col-sm-6 ">
    <div style="margin-left: 20%; margin-right: 20%;">
      <div class="text-white text-center d-flex align-items-center">
        <a style="background-image: url('https://cdn.pixabay.com/photo/2016/01/19/17/15/wind-turbine-1149604__340.jpg'); background-repeat: no-repeat; background-size: cover; height: 400px; width: 100%; background-position: center;" class="responsive" href="<?php echo base_url() ?>index.php/Consommation/consommation_mensuelle">
    </a>
        
      </div>
      <a href="<?php echo base_url() ?>index.php/Consommation/consommation_mensuelle" class="btn btn-primary my-3 mb-5 btn-block">Renseigner une consommation mensuelle</a>
    </div>
  </div>

  <div class="col-sm-6 ">
    <div style="margin-left: 20%; margin-right: 20%; ">
      <div class="text-white text-center d-flex align-items-center">
        <a style="background-image: url('https://cdn.pixabay.com/photo/2016/12/23/18/04/communication-1927697_1280.png'); background-repeat: no-repeat; background-size: cover; height: 400px; width: 100%; background-position: center;" href="<?php echo base_url() ?>index.php/Equipement/afficherEquipements">
    </a>
        
      </div>
      <a href="<?php echo base_url() ?>index.php/Equipement/afficherEquipements" class="btn btn-primary my-3 mb-5 btn-block">Gérer mes équipements</a>
    </div>
  </div>

  <div class="col-sm-6">
    <div style="margin-left: 20%; margin-right: 20%;">
      <div class="d-flex">
        <a style="background-image: url('https://cdn.pixabay.com/photo/2015/10/22/17/45/leaf-1001679__340.jpg'); background-repeat: no-repeat; background-size: cover; height: 400px; width: 100%;background-position: center;" href="<?php echo base_url() ?>index.php/Consommation/maintenance" >
    </a>
       
      </div>
       <a href="#" class="btn btn-primary my-3 mb-5 btn-block">Mes consommation mensuelles</a>
    </div>
  </div>
</div>

