<?php

if (!isset($_SESSION['Particulier'])) {
  header("Location: ".base_url()."index.php/");
    exit();
}
?>


<div>
	<div class="text-center">
		<h1>Paramètres du compte</h1>
	</div>
</div>

<div class="container" >

	<div class="text-left my-5">
		<a href="<?php echo base_url() ?>index.php/Compte/afficherCompte">Mon compte</a> > Paramètres du compte
	</div>

	<?php echo form_open('Compte/update_data'); ?>
	<form>
		
	  <div class="form-group row">
	    <label for="nom" class="col-sm-3 col-form-label">Nom</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="nom" name="nom" value="<?php echo $comptes['com_nom']; ?>">
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="prenom" class="col-sm-3 col-form-label">Prénom</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="prenom" name="prenom" value="<?php echo $comptes['com_prenom']; ?>">
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="e-mail" class="col-sm-3 col-form-label">Mail</label>
	    <div class="col-sm-5">
	      <input type="Mail" class="form-control" id="e-mail" name="e-mail" value="<?php echo $comptes['com_mail']; ?>">
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="typeCompte" class="col-sm-3 col-form-label">Type de compte</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="typeCompte" disabled="true" value="<?php echo $comptes['typCom_nom']; ?>">
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="pseudo" class="col-sm-3 col-form-label">Pseudo</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="pseudo" disabled="true" name="pseudo" value="<?php echo $comptes['com_pseudo']; ?>">
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="ancienMotDePasse" class="col-sm-3 col-form-label">Ancien mot de passe</label>
	    <div class="col-sm-5">
	      <input type="password" class="form-control" name="ancienMotDePasse" id="ancienMotDePasse">
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="nouveauMotDePasse" class="col-sm-3 col-form-label">Nouveau mot de passe</label>
	    <div class="col-sm-5">
	      <input type="password" class="form-control" name="nouveauMotDePasse" id="nouveauMotDePasse">
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="confirmationNouveauMotDePasse" class="col-sm-3 col-form-label">Confirmer le nouveau mot de passe</label>
	    <div class="col-sm-5">
	      <input type="password" class="form-control btn-lg" name="confirmationNouveauMotDePasse" id="confirmationNouveauMotDePasse">
	    </div>
	  </div>
	 
	  <div class="form-group row my-5">
	    <div class="col-sm-10 text-center">
	      <button type="submit" class="btn btn-primary">Sauvegarder les modifications</button>
	    </div>
	  </div>
	</form>

</div>